package io.famix;


import model.famix.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.*;

import java.io.File;
import java.io.FileInputStream;
import java.util.*;

/**
 * Created by nw91 on 07/10/2015.
 */
public class FamixPackageReader {

    protected Map<FamixClass,String> subToSuperClass = new HashMap<FamixClass,String>();
    protected Map<FamixClass,ClassNode> famixClasses = new HashMap<FamixClass,ClassNode>();
    protected Map<String,FamixPackage> packages = new HashMap<String,FamixPackage>();
    protected FamixCallGraph callGraph;


    public FamixPackageReader(String root){

        String packageRoot = root;

        File f = new File(packageRoot); // root directory;
        Iterator<File> fileIt = FileUtils.iterateFiles(f, new WildcardFileFilter("*.class"), new WildcardFileFilter("*"));
        while(fileIt.hasNext()){
            File currentClass = fileIt.next();
           String pkgPath = (currentClass.getParent());
            FamixPackage pkg = packages.get(pkgPath);
            if(pkg == null){
                pkg = new FamixPackage(pkgPath);
                packages.put(pkgPath, pkg);
            }
            pkg.addClass(generateClass(currentClass));
        }
        for(FamixClass fc : famixClasses.keySet()){
            ClassNode cls = famixClasses.get(fc);
            fc.setParent(findFamixClass(cls.superName));
            for(Object fn : cls.fields){
                FieldNode node = (FieldNode)fn;
                fc.addAttribute(generateAttribute(node));
            }
            for(Object mn : cls.methods){

                fc.addMethod(generateMethod(mn));
            }
        }
        for(FamixClass fc : famixClasses.keySet()){
            ClassNode cls = famixClasses.get(fc);
            for(Object mn :cls.methods){
                MethodNode nd = (MethodNode)mn;
                FamixMethod fm = fc.getMethod(nd.name);
                InsnList ilist = nd.instructions;
                for(int i = 0; i< ilist.size(); i++){
                    AbstractInsnNode instruction = ilist.get(i);
                    if(instruction instanceof MethodInsnNode){
                        MethodInsnNode invoke = (MethodInsnNode) instruction;
                        FamixClass targetClass = findFamixClass(invoke.owner);
                        if(targetClass!=null) {
                            FamixMethod targetMethod = targetClass.getMethod(invoke.name);
                            while(targetMethod == null && targetClass.getParent()!=null){
                                targetClass = targetClass.getParent();
                                targetMethod = targetClass.getMethod(invoke.name);
                            }
                            if(targetMethod != null)
                                fm.addCallee(targetMethod);
                        }
                    }
                }
            }
        }
        for(FamixClass fc : famixClasses.keySet()){
            fc.addMetric("NOA",(double)fc.getAttributes().size());
            fc.addMetric("NOM",(double)fc.getMethods().size());
            fc.addMetric("LOC",totalLoc(fc.getMethods()));
        }
        callGraph = new FamixCallGraph(famixClasses.keySet());
    }

    public FamixCallGraph getCallGraph() {
        return callGraph;
    }

    public Collection<FamixClass> getClasses(){
        return famixClasses.keySet();
    }

    private static double totalLoc(Collection<FamixMethod> methods) {
        double total = 0D;
        for(FamixMethod fm : methods){
            total+=fm.getMetricValue("LOC");
        }
        return total;
    }

    private FamixAttribute generateAttribute(FieldNode fn) {
        return new FamixAttribute(fn.name, findFamixClass(fn.desc));
    }

    private  FamixClass findFamixClass(String desc) {
        FamixClass ret = null;
        for(FamixClass fc : famixClasses.keySet()){
            if(fc.getName().equals(desc)) {
                ret = fc;
                break;
            }
        }
        return ret;
    }

    private static FamixMethod generateMethod(Object mn) {
        MethodNode node = (MethodNode)mn;
        FamixMethod fm = new FamixMethod(node.name);
        fm.addMetric("LOC",(double)node.instructions.size());
        return fm;
    }

    private  FamixClass generateClass(File currentClass) {
        FamixClass fc = null;

        try {
            FileInputStream fis = new FileInputStream(currentClass);
            ClassReader cr = new ClassReader(fis);
            ClassNode cn = new ClassNode();
            cr.accept(cn,0);
            fc = new FamixClass(cn.name);
            famixClasses.put(fc,cn);
            subToSuperClass.put(fc,cn.superName);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return fc;

    }

    public Map<String, FamixPackage> getPackages() {
        return packages;
    }
}
