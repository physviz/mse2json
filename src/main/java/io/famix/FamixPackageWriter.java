package io.famix;

import model.Entity;
import model.famix.FamixCallGraph;
import model.famix.FamixClass;
import model.famix.FamixMethod;
import model.famix.FamixPackage;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Collection;

/**
 * Created by neilwalkinshaw on 19/10/2015.
 */
public class FamixPackageWriter {



    public FamixPackageWriter(Collection<FamixPackage> packages, String mseFile, FamixCallGraph callGraph){
        String toWrite = buildString(packages, callGraph);
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(mseFile, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.println(toWrite);
        writer.close();
    }

    private String buildString(Collection<FamixPackage> packages, FamixCallGraph callGraph) {
        String toWrite = "((FAMIX.JavaSourceLanguage)\n";
        for(FamixPackage pkg : packages){
            toWrite+= getPackageString(pkg)+"\n";
            for(FamixClass cls : pkg.getClasses()){
                toWrite+= getClassString(cls, pkg)+"\n";
                String inheritance = getInheritance(cls);
                if(inheritance !=null){
                    toWrite+= inheritance + "\n";
                }
                for(FamixMethod method : cls.getMethods()){
                    toWrite += methodString(method, cls);
                }
            }
        }
        for(FamixMethod method : callGraph.getMethods()){
            for(FamixMethod target : callGraph.getDestinations(method)){
                toWrite += invocation(method,target);
            }
        }
        return toWrite+")";
    }

    private String invocation(FamixMethod method, FamixMethod target) {
        String invocation = "(FAMIX.Invocation\n";
        invocation+="(sender (ref:"+method.getId()+"))\n";
        invocation+="(candidates (ref:"+target.getId()+"))\n";
        invocation+="(signature '"+target.getName()+"')\n)\n\n";

        return invocation;
    }


    private String methodString(FamixMethod method, FamixClass owner) {
        String retString = "(FAMIX.Method (id: "+method.getId()+")\n";
        retString +="(name '"+method.getName().replaceAll("\\.",":")+"')\n";
        retString +="(parentType (ref:"+owner.getId()+"))\n";
        for(String metric : method.availableMetrics()){
            Double val = method.getMetricValue(metric);
            retString += "("+metric+" "+val+")\n";
        }
        retString += "\n)\n\n";
        return retString;
    }

    private String getInheritance(FamixClass cls) {
        if(cls.getParent() ==null){
            return null;
        }

        String retString = "(FAMIX.Inheritance (id: "+ Entity.idCounter++ + ")\n";
        retString += "(subclass (ref:"+cls.getId()+"))\n";
        retString += "(superclass (ref:"+cls.getParent().getId()+"))\n\n)";
        return retString;

    }

    private String getPackageString(FamixPackage pkg) {
        String retString = "(FAMIX.Package (id: "+pkg.getId()+")\n";
        retString +="(name '"+pkg.getName().replaceAll("\\.",":")+"')\n)\n\n";
        return retString;
    }

    private String getClassString(FamixClass cls, FamixPackage pkg) {
        String retString = "(FAMIX.Class (id: "+cls.getId()+")\n";
        retString +="(name '"+cls.getName().replaceAll("\\.",":")+"')\n";
        retString +="(parentPackage (ref:  "+pkg.getId()+"))\n";
        for(String metric : cls.availableMetrics()){
            Double val = cls.getMetricValue(metric);
            retString += "("+metric+" "+val+")\n";
        }
        retString += "\n)\n\n";
        return retString;
    }


}
