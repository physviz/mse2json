import javax.json.*;
import javax.json.stream.JsonGenerator;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class MSE2Json {

	protected MSEFile mse;
	protected JsonBuilderFactory jbf;
	
	public MSE2Json(MSEFile mse){
		this.jbf = Json.createBuilderFactory(null);
		this.mse = mse;
		OutputStream os;
		try {
			Map<String, Object> properties = new HashMap<String,Object>(1);
            properties.put(JsonGenerator.PRETTY_PRINTING, true);
            JsonWriterFactory jw = Json.createWriterFactory(properties);
            os = new FileOutputStream(mse.fileName.substring(0,mse.fileName.indexOf(""))+".json");
            JsonWriter jg = jw.createWriter(os);
			
			JsonObjectBuilder nodesAndRels = jbf.createObjectBuilder();
			nodesAndRels.add("nodes", buildNodes());
			nodesAndRels.add("relations", buildRelations());
			jg.writeObject(nodesAndRels.build());
			jg.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	private JsonArray buildRelations() {
		JsonArrayBuilder relBuilder = jbf.createArrayBuilder();
		for(Integer method: mse.getMethodsToClasses().keySet()){
            Integer parent =  mse.getMethodsToClasses().get(method);
            String parentName = mse.getClasses().get(parent);

			JsonObjectBuilder parentBuilder = jbf.createObjectBuilder();
			String from = getMethodSignature(method);
			String to = mse.getClasses().get(parent);

			if(from == null || to == null)
				continue;
            /*if(from.contains("_unknown_type"))
                continue;
            if(to.contains("_unknown_type"))
                continue;*/
			parentBuilder.add("from", from);
			parentBuilder.add("to", to);
			parentBuilder.add("type", "belongsTo");
			relBuilder.add(parentBuilder.build());
		}
		for(Integer cl: mse.getClassesToParents().keySet()){
			Integer parent = mse.getClassesToParents().get(cl);

			if(parent == null)
				continue;
			JsonObjectBuilder parentBuilder = jbf.createObjectBuilder();
			String from = mse.getClasses().get(cl);
			String to = mse.getClasses().get(parent);
			if(from == null || to == null)
				continue;
            /*if(from.contains("_unknown_type"))
                continue;
            if(to.contains("_unknown_type"))
                continue;*/

			parentBuilder.add("from",from);
			parentBuilder.add("to", to);
			parentBuilder.add("type", "inheritsFrom");
			relBuilder.add(parentBuilder.build());
		}
		
		for(Integer me: mse.getMethodCalls().keySet()){
			Set<Integer> dests = mse.getMethodCalls().get(me);

			for(Integer d : dests){

				String from = getMethodSignature(me);
				String to = getMethodSignature(d);

				if(from == null || to == null)
					continue;
                /*if( from.contains("_unknown_type"))
                    continue;
                if(to.contains("_unknown_type"))
                    continue;*/
                JsonObjectBuilder callBuilder = jbf.createObjectBuilder();
				callBuilder.add("from",from);
				callBuilder.add("to", to);
                if(!mse.getMethodsToClasses().get(me).equals(mse.getMethodsToClasses().get(d)))
				    callBuilder.add("type", "interCalls");
                else
                    callBuilder.add("type", "intraCalls");
				relBuilder.add(callBuilder.build());
			}
			
		}

        for(Integer me: mse.getAttributes().keySet()){

            Integer parentClass = mse.getAttributesToParents().get(me);

            String from = mse.getClasses().get(parentClass);
            String to = getAttributeSignature(me);

            if(from == null || to == null)
                continue;
            /*if( from.contains("_unknown_type"))
                continue;
            if(to.contains("_unknown_type"))
                continue;*/

            JsonObjectBuilder callBuilder = jbf.createObjectBuilder();
            callBuilder.add("from",from);
            callBuilder.add("to", to);
            callBuilder.add("type", "attribute");
            relBuilder.add(callBuilder.build());


        }
        for(Integer me: mse.getAttributes().keySet()){

            Integer typeClass = mse.getAttributesToTypes().get(me);

            String from = getAttributeSignature(me);
            String to = mse.getClasses().get(typeClass);

            if(from == null || to == null)
                continue;
           /* if( from.contains("_unknown_type"))
                continue;
            if(to.contains("_unknown_type"))
                continue;*/

            JsonObjectBuilder callBuilder = jbf.createObjectBuilder();
            callBuilder.add("from",from);
            callBuilder.add("to", to);
            callBuilder.add("type", "attributeType");
            relBuilder.add(callBuilder.build());


        }
        for(Integer me: mse.getClasses().keySet()){


            String to = mse.getClasses().get(me);
            String from = mse.getFileName();

            if(from == null || to == null)
                continue;
           /* if( from.contains("_unknown_type"))
                continue;
            if(to.contains("_unknown_type"))
                continue;*/
            JsonObjectBuilder callBuilder = jbf.createObjectBuilder();
            callBuilder.add("from",from);
            callBuilder.add("to", to);
            callBuilder.add("type", "project");
            relBuilder.add(callBuilder.build());


        }
		return relBuilder.build();
	}
	
	private String getMethodSignature(Integer method){
		Integer cl = mse.getMethodsToClasses().get(method);
		String ret = mse.getClasses().get(cl);
		if(ret == null)
			return null;
		ret = ret + "" + mse.getMethods().get(method);
		return ret;
	}

	private JsonArray buildNodes() {
		JsonArrayBuilder nodeBuilder = jbf.createArrayBuilder();
        nodeBuilder.add(createProjectJsonObject());
		addClasses(nodeBuilder);
		addMethods(nodeBuilder);
        addAttributes(nodeBuilder);
		return nodeBuilder.build();
	}

    private void addAttributes(JsonArrayBuilder nodeBuilder) {
        for(Integer cl : mse.getAttributes().keySet()){
            Integer parentClass = mse.getAttributesToParents().get(cl);
            String parent = mse.getClasses().get(parentClass);
            if(parent == null)
                continue;
            //if(parent.contains("_unknown_type"))
              //  continue;
            nodeBuilder.add(createAttributeJsonObject(cl));
        }

    }

	private void addMethods(JsonArrayBuilder nodeBuilder) {
		for(Integer cl : mse.getMethods().keySet()){
            Integer classInt = mse.getMethodsToClasses().get(cl);
            String name = mse.getClasses().get(classInt);
            if(name == null)
                continue;
            //if(name.contains("_unknown"))
              //  continue;
			nodeBuilder.add(createMethodJsonObject(cl));
		}
		
	}

    private JsonValue createProjectJsonObject() {
        JsonObjectBuilder attBuilder = jbf.createObjectBuilder();
        attBuilder.add("name", mse.getFileName());
        attBuilder.add("type", "project");
        attBuilder.add("attributes",jbf.createArrayBuilder());
        return attBuilder.build();
    }

	private JsonValue createAttributeJsonObject(Integer cl) {
		JsonObjectBuilder attBuilder = jbf.createObjectBuilder();
        attBuilder.add("name", getAttributeSignature(cl));
        attBuilder.add("type", "classAttribute");
        attBuilder.add("attributes",jbf.createArrayBuilder());
		return attBuilder.build();
	}

    private JsonValue createMethodJsonObject(Integer cl) {
        JsonObjectBuilder methodBuilder = jbf.createObjectBuilder();
        methodBuilder.add("name", getMethodSignature(cl));
        methodBuilder.add("type", "method");
        methodBuilder.add("attributes", createMethodAttributesJsonArray(cl));
        return methodBuilder.build();
    }

    private String getAttributeSignature(Integer att){
        Integer cl = mse.getAttributesToParents().get(att);
        String ret = mse.getClasses().get(cl);
        if(ret == null)
            return null;
        ret = ret + "" +mse.getAttributes().get(att);
        return ret;
    }

	private JsonValue createMethodAttributesJsonArray(Integer cl) {
		JsonArrayBuilder attributeBuilder = jbf.createArrayBuilder();
		
		Set<Metric> methodMetrics = mse.getMethodMetrics().get(cl);
		if(methodMetrics != null){
			JsonArrayBuilder metricArrayBuilder = jbf.createArrayBuilder();
			for(Metric m : methodMetrics){
				JsonObjectBuilder metricBuilder = jbf.createObjectBuilder();
				metricBuilder.add(m.getName(), m.getValue());
				metricArrayBuilder.add(metricBuilder);
			}
			attributeBuilder.add(metricArrayBuilder);
		}
		return attributeBuilder.build();
	}

	private void addClasses(JsonArrayBuilder nodeBuilder) {
		for(Integer cl : mse.getClasses().keySet()){
            //if(mse.getClasses().get(cl).contains("_unknown_type"))
              //  continue;
			nodeBuilder.add(createClassJsonObject(cl));
		}
		
	}

	private JsonObject createClassJsonObject(Integer cl) {
		JsonObjectBuilder classBuilder = jbf.createObjectBuilder();
		classBuilder.add("name", mse.getClasses().get(cl));
		classBuilder.add("type", "class");
		classBuilder.add("attributes", createClassAttributesJsonArray(cl));
		return classBuilder.build();
	}

	private JsonArray createClassAttributesJsonArray(Integer cl) {
		JsonArrayBuilder attributeBuilder = jbf.createArrayBuilder();
		Set<Metric> classMetrics = mse.getClassMetrics().get(cl);
		if(classMetrics != null){
			JsonArrayBuilder metricArrayBuilder = jbf.createArrayBuilder();
			for(Metric m : classMetrics){
				JsonObjectBuilder metricBuilder = jbf.createObjectBuilder();
				metricBuilder.add(m.getName(), m.getValue());
				metricArrayBuilder.add(metricBuilder);
			}
			attributeBuilder.add(metricArrayBuilder);
		}
		return attributeBuilder.build();
	}

    public static void main(String[] args){
        try {
            MSEFile mse = new MSEFile(args[0]);
            MSE2Json msej = new MSE2Json(mse);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
	
}
