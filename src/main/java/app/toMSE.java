package app;

import io.famix.FamixPackageReader;
import io.famix.FamixPackageWriter;

/**
 * Created by neilwalkinshaw on 19/10/2015.
 */
public class toMSE {

    public static void main(String[] args){
        FamixPackageReader fpb = new FamixPackageReader(args[0]);
        FamixPackageWriter fpw = new FamixPackageWriter(fpb.getPackages().values(),"msefile.mse",fpb.getCallGraph());
    }
}
