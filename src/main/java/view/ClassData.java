package view;

import io.famix.FamixPackageReader;
import model.famix.FamixClass;

import java.util.Collection;

/**
 * Created by nw91 on 07/10/2015.
 */
public class ClassData {

    public ClassData(Collection<FamixClass> classes){
        boolean headerDone = false;
        int cols = 0;
        for(FamixClass cls : classes){
            if(!headerDone){
                headerDone = true;
                Collection<String> availableMetrics = cls.availableMetrics();
                String header = "";
                for(String hd : availableMetrics) {
                    header+=","+hd;
                }
                System.out.println(header);
            }
            String clsString = cls.getName();
            for(String metrics : cls.availableMetrics()){
                clsString+=","+cls.getMetricValue(metrics);
            }
            System.out.println(clsString);

        }
    }

    public static void main(String[] args){
        FamixPackageReader fpb = new FamixPackageReader(args[0]);
        ClassData cd = new ClassData(fpb.getClasses());
    }

}
