package view;

import io.famix.FamixPackageReader;
import model.famix.FamixClass;
import model.famix.FamixMethod;

import java.util.Collection;

/**
 * Created by nw91 on 07/10/2015.
 */
public class MethodData {

    public MethodData(Collection<FamixClass> classes){
        boolean headerDone = false;
        int cols = 0;
        for(FamixClass cls : classes){
            for(FamixMethod meth : cls.getMethods()) {
                if (!headerDone) {
                    headerDone = true;
                    Collection<String> availableMetrics = meth.availableMetrics();
                    String header = "";
                    for (String hd : availableMetrics) {
                        header += "," + hd;
                    }
                    System.out.println(header);
                }
                String clsString = cls.getName()+"."+meth.getName();
                for (String metrics : meth.availableMetrics()) {
                    clsString += "," + meth.getMetricValue(metrics);
                }

                System.out.println(clsString);
            }
        }
    }

    public static void main(String[] args){
        FamixPackageReader fpb = new FamixPackageReader(args[0]);
        MethodData cd = new MethodData(fpb.getClasses());
    }

}
