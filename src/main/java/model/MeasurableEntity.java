package model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by nw91 on 07/10/2015.
 */
public abstract class MeasurableEntity extends Entity{

    Map<String,Double> metrics;

    public MeasurableEntity(String identifier){
        super(identifier);
        metrics = new HashMap<String,Double>();
    }

    public void addMetric(String name, Double value){
        metrics.put(name,value);
    }

    public double getMetricValue(String name){
        return metrics.get(name);
    }

    public Collection<String> availableMetrics(){
        return metrics.keySet();
    }

}
