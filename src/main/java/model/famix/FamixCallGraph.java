package model.famix;

import org.jgrapht.graph.ClassBasedEdgeFactory;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedMultigraph;

import java.util.Collection;
import java.util.HashSet;

/**
 * Created by neilwalkinshaw on 07/10/15.
 */


public class FamixCallGraph {
    protected static int edges = 0;


    protected DirectedMultigraph<FamixMethod, DefaultEdge> callGraph;

    public FamixCallGraph(Collection<FamixClass> classes){
        callGraph = new DirectedMultigraph<FamixMethod, DefaultEdge>(new ClassBasedEdgeFactory<FamixMethod, DefaultEdge>(DefaultEdge.class));
        for(FamixClass fc : classes){
            for(FamixMethod fm : fc.getMethods()){
                callGraph.addVertex(fm);
                for(FamixMethod callee: fm.getCallees()){

                    if(!callee.equals(fm)) {
                        callGraph.addVertex(callee);
                        callGraph.addEdge(fm, callee);
                    }
                }
            }
        }
        for(FamixMethod node : callGraph.vertexSet()){
            double incoming = (double)callGraph.incomingEdgesOf(node).size();
            double outgoing = (double)callGraph.outgoingEdgesOf(node).size();
            node.addMetric("FANIN",incoming);
            node.addMetric("FANOUT",outgoing);
        }
        for(FamixClass fc : classes) {
            double incoming = 0;
            double outgoing = 0;
            for(FamixMethod fm : fc.getMethods()) {
                incoming += fm.getMetricValue("FANIN");
                outgoing += fm.getMetricValue("FANOUT");
            }
            fc.addMetric("FANIN",incoming);
            fc.addMetric("FANOUT",outgoing);
        }
    }

    public Collection<FamixMethod> getMethods(){
        return callGraph.vertexSet();
    }

    public Collection<FamixMethod> getDestinations(FamixMethod node){
        Collection<FamixMethod> targets = new HashSet<FamixMethod>();
        for(DefaultEdge outgoing : callGraph.outgoingEdgesOf(node)){
            targets.add(callGraph.getEdgeTarget(outgoing));
        }
        return targets;
    }
}
