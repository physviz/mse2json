package model.famix;

import model.MeasurableEntity;

import java.util.Collection;
import java.util.HashSet;

/**
 * Created by nw91 on 07/10/2015.
 */
public class FamixMethod extends MeasurableEntity {

    Collection<FamixMethod> callees;

    public FamixMethod(String identifier) {
        super(identifier);
        callees = new HashSet<FamixMethod>();
    }

    public void addCallee(FamixMethod m){
        callees.add(m);
    }

    public Collection<FamixMethod> getCallees(){
        return callees;
    }

}
