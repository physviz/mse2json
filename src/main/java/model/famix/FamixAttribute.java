package model.famix;

import model.Entity;

/**
 * Created by nw91 on 07/10/2015.
 */
public class FamixAttribute extends Entity {

    public FamixClass type;

    public FamixAttribute(String name, FamixClass type) {
        super(name);
        this.type = type;
    }

    public FamixClass getType(){
        return type;
    }
}
