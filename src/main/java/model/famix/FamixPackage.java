package model.famix;

import model.Entity;

import java.util.Collection;
import java.util.HashSet;

/**
 * Created by nw91 on 07/10/2015.
 */
public class FamixPackage extends Entity{

    protected Collection<FamixClass> classes;


    public FamixPackage(String name) {
        super(name);
        classes = new HashSet<FamixClass>();
    }

    public void addClass(FamixClass cl){
        classes.add(cl);
    }

    public Collection<FamixClass> getClasses(){
        return classes;
    }




}
