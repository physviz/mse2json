package model.famix;

import model.MeasurableEntity;

import java.util.Collection;
import java.util.HashSet;

/**
 * Created by nw91 on 07/10/2015.
 */
public class FamixClass extends MeasurableEntity {

    protected Collection<FamixMethod> methods;
    protected Collection<FamixAttribute> attributes;

    protected FamixClass parent;

    public FamixClass getParent() {
        return parent;
    }

    public void setParent(FamixClass parent) {
        this.parent = parent;
    }

    public FamixClass(String identifier) {
        super(identifier);
        methods = new HashSet<FamixMethod>();
        attributes = new HashSet<FamixAttribute>();
    }

    public void addMethod(FamixMethod m){
        methods.add(m);
    }

    public void addAttribute(FamixAttribute a){
        attributes.add(a);
    }

    public FamixMethod getMethod(String name){
        for(FamixMethod fm : methods){
            if(fm.getName().equals(name))
                return fm;
        }
        return null;
    }

    public Collection<FamixMethod> getMethods() {
        return methods;
    }

    public Collection<FamixAttribute> getAttributes() {
        return attributes;
    }
}
