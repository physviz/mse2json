package model;

/**
 * Created by nw91 on 07/10/2015.
 */
public abstract class Relationship <F extends Entity,T extends Entity> {

    protected F from;
    protected T to;

    public Relationship(F from, T to) {
        this.from = from;
        this.to = to;
    }
}
