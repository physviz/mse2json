package model;

/**
 * Created by nw91 on 07/10/2015.
 */
public abstract class Entity {

    public static int idCounter = 0;

    protected int id;

    protected String name;

    public Entity(String name){
        this.name = name;
        this.id = idCounter++;
    }

    public String getName(){
        return name;
    }

    public int getId() {
        return id;
    }
}
