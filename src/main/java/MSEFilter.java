import java.io.*;


public class MSEFilter {


	public MSEFilter(String mseFile, String filter) throws IOException{
        BufferedReader br = new BufferedReader(new FileReader(mseFile));
        BufferedWriter bw = new BufferedWriter(new FileWriter("filtered.mse"));
	    try {
	        String line = br.readLine();
	        while (line != null) {
	            if(line.contains(filter))
	            	skip(br);
	            else  {
                    bw.write(line);
                    bw.newLine();
                }
	            line = br.readLine();
	        }
	        
	    } finally {
	        br.close();
            bw.close();
	    }
	}

    private void skip(BufferedReader br) throws IOException {

        String line = br.readLine();
        while(!line.trim().equals(")")){
            line = br.readLine();
        }
        br.readLine();
    }



	public static void main(String[] args){
        String mseFile = args[0];
        String filterString = args[1];
        try {
            MSEFilter msef = new MSEFilter(mseFile,filterString);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
