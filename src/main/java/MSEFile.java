import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class MSEFile {
	
	Map<Integer,String> classes = new HashMap<Integer,String>();
	Map<Integer,String> methods= new HashMap<Integer,String>();
    Map<Integer,Integer> classesToPackages= new HashMap<Integer,Integer>();
    Map<Integer,String> packages= new HashMap<Integer,String>();
	Map<Integer,Integer> methodsToClasses= new HashMap<Integer,Integer>();
	Map<Integer,Set<Integer>> methodCalls = new HashMap<Integer,Set<Integer>>();
	Map<Integer,Integer> classesToParents= new HashMap<Integer,Integer>();
	Map<Integer,Set<Metric>> classMetrics= new HashMap<Integer,Set<Metric>>();
	Map<Integer,Set<Metric>> methodMetrics= new HashMap<Integer,Set<Metric>>();
    Map<Integer,Integer> attributesToParents= new HashMap<Integer,Integer>();
    Map<Integer,Integer> attributesToTypes= new HashMap<Integer,Integer>();
    Map<Integer,String> attributes= new HashMap<Integer,String>();
    Map<Integer,Integer> methodsToParameters= new HashMap<Integer,Integer>();
    String fileName;
	
	public MSEFile(String mseFile) throws IOException{
        fileName = mseFile;
		BufferedReader br = new BufferedReader(new FileReader(mseFile));
	    try {
	        String line = br.readLine();
	        while (line != null) {
	            if(line.contains("FAMIX.Class") || line.contains("FAMIX.ParameterizableClass"))
	            	readClass(line,br);
	            else if(line.contains("FAMIX.Inheritance"))
	            	readInheritance(line,br);
                else if(line.contains("FAMIX.Attribute"))
                    readAttribute(line, br);
                else if(line.contains("FAMIX.Package"))
                    readPackage(line, br);
	            else if(line.contains("FAMIX.Method"))
	            	readMethod(line, br);
	            else if(line.contains("FAMIX.Invocation"))
	            	readInvocation(line,br);
                else if(line.contains("FAMIX.Parameter"))
                    readParameter(line, br);
	            line = br.readLine();
	        }
	        
	    } finally {
	        br.close();
	    }
        for(Integer cls : classes.keySet()){
            String pck = packages.get(classesToPackages.get(cls));
            classes.put(cls, pck+":"+classes.get(cls));

            if(!classesToParents.containsKey(cls)){

                addMetric(new Metric(cls, "ISROOT", 1D), classMetrics);
                continue;
            }
            else{
                assert(classesToParents.get(cls) != null);
            }
            Integer parent = classesToParents.get(cls);
            assert(parent!=null);
            assert(classes.containsKey(parent));
            addMetric(new Metric(cls, "ISROOT", 0D), classMetrics);



        }
	}

    public String getFileName(){
        return fileName;
    }

    private void readParameter(String line, BufferedReader br) throws IOException {
        Integer fromMethod = null;
        Integer toClass = null;
        line = br.readLine();
        while(!line.trim().equals(")")){
            if(getAttribute(line).equals("parentBehaviouralEntity"))
                fromMethod = Integer.valueOf(extractRef(line));
            else if(getAttribute(line).equals("declaredType"))
                toClass = Integer.valueOf(extractRef(line));
            line = br.readLine();
        }
        methodsToParameters.put(fromMethod,toClass);
    }

    private void readAttribute(String line, BufferedReader br) throws IOException{
        Integer parentClass = null;
        Integer typeClass = null;
        String name = null;
        int id = getID(line);
        line = br.readLine();
        while(!line.trim().equals(")")){
            if(getAttribute(line).equals("parentType"))
                parentClass = Integer.valueOf(extractRef(line));
            else if(getAttribute(line).equals("declaredType"))
                typeClass = Integer.valueOf(extractRef(line));
            else if(getAttribute(line).equals("name")) {
                int sigIndex = line.indexOf("name '");
                int end = line.indexOf("')");
                int start = sigIndex + 6;
                name = line.substring(start, end);
            }
            line = br.readLine();
        }
        attributes.put(id,name);
        attributesToParents.put(id,parentClass);
        attributesToTypes.put(id,typeClass);
    }

    private void readPackage(String line, BufferedReader br) throws IOException{
        String name = null;
        int id = getID(line);
        line = br.readLine();
        while(!line.trim().equals(")")){
             if(getAttribute(line).equals("name")) {
                int sigIndex = line.indexOf("name '");
                int end = line.indexOf("')");
                int start = sigIndex + 6;
                name = line.substring(start, end);
            }
            line = br.readLine();
        }
        packages.put(id,name);
    }



    private String getAttribute(String line){
		line = line.trim();
		String attr = line.substring(1,line.indexOf(" "));
		return attr;
	}

	private void readInvocation(String line, BufferedReader br) throws IOException {
		Integer sender = null;
		Integer receiver = null;
		line = br.readLine();
		while(!line.trim().equals(")")){
			if(getAttribute(line).equals("sender"))
				sender = Integer.valueOf(extractRef(line));
			else if(getAttribute(line).equals("candidates"))
				receiver = Integer.valueOf(extractRef(line));
			line = br.readLine();
		}
		addInvocation(sender,receiver);
		

		
	}

	private void addInvocation(Integer sender, Integer receiver) {
		Set<Integer> targets = methodCalls.get(sender);
		if(targets == null){
			targets = new HashSet<Integer>();
			methodCalls.put(sender, targets);
		}
		targets.add(receiver);
		
	}

	private void readMethod(String line, BufferedReader br) throws IOException {
		int id = getID(line);
		line = br.readLine();
		while(!line.trim().equals(")")){
			if(getAttribute(line).equals("signature"))
				addMethodSignature(id,line);
			else{
				String attribute = getAttribute(line);
				if(isUpper(attribute)){ //metric
					addMetric(id,attribute, line, methodMetrics);
				}
				else if(attribute.equals("parentType")){
					String parent = extractRef(line);
					Integer p = Integer.valueOf(parent);
					methodsToClasses.put(id, p);
				}
			}
			line = br.readLine();
		}
	}
	
	private void addMetric(int entity, String attribute, String line, Map<Integer, Set<Metric>> metrics) {
		String val = extractValue(line);
		Double metric = Double.valueOf(val);
		Metric m = new Metric(entity, attribute, metric);
		addMetric(m, metrics);

	}

	protected String extractValue(String line) {
		String val = line.trim().substring(line.indexOf(' '), line.indexOf(')')-1);
		return val;
	}
	
	protected String extractRef(String line) {
		String val = line.trim().substring(line.indexOf("(ref:")+5, line.indexOf(')')-1);
		return val;
	}

	private void addMetric(Metric m, Map<Integer, Set<Metric>> collection) {
		Set<Metric> existing = collection.get(m.getEntity());
		if(existing == null){
			existing = new HashSet<Metric>();
			collection.put(m.getEntity(), existing);
		}
		existing.add(m);
		
	}

	private static boolean isUpper(String s)
	{
	    for(char c : s.toCharArray())
	    {
	        if(! Character.isUpperCase(c))
	            return false;
	    }

	    return true;
	}

	private void addMethodSignature(int id,String line) {
		int sigIndex = line.indexOf("signature '");
		int end = line.indexOf("')");
		int start = sigIndex + 11;
		methods.put(id, line.substring(start, end));
		
	}
	
	private String readClassName(String line) {
		int sigIndex = line.indexOf("name '");
		int end = line.indexOf("')");
		int start = sigIndex + 6;
        return line.substring(start, end);

		
	}

	private int getID(String line) {
		int start = line.indexOf("id:")+3;
		int end = line.indexOf(')', start);
		String val = line.substring(start, end).trim();
		return Integer.valueOf(val);
	}

	private void readInheritance(String line, BufferedReader br) throws IOException {
		Integer subClass = null;
		Integer superClass = null;
		line = br.readLine();
		while(!line.trim().equals(")")){
			if(getAttribute(line).equals("subclass"))
				subClass = Integer.valueOf(extractRef(line));
			else if(getAttribute(line).equals("superclass"))
				superClass = Integer.valueOf(extractRef(line));
			line = br.readLine();
		}
        if(classes.containsKey(subClass) && classes.containsKey(superClass)) {
            classesToParents.put(subClass, superClass);
        }
        //
	}

	private void readClass(String line, BufferedReader br) throws IOException {
		int id = getID(line);
		line = br.readLine();
        boolean isStubOrInterface = false;
        boolean added = false;
        String className = null;
		while(!line.trim().equals(")")){
            if(isStubOrInterface) {
                line = br.readLine();
            }
            else {
                if (line.contains("isStub") || line.contains("isInterface")) {
                    isStubOrInterface = true;
                    continue;
                }
                if (line.contains("parentPackage")) {
                    classesToPackages.put(id,Integer.parseInt(extractRef(line)));
                }
                if (getAttribute(line).equals("name"))
                    className = readClassName(line);
                else {
                    String attribute = getAttribute(line);
                    if (isUpper(attribute)) { //metric
                        if(!added){
                            classes.put(id, className);
                            added = true;
                        }
                        addMetric(id, attribute, line, classMetrics);
                    }


                }
                line = br.readLine();
            }

		}
		
	}

	public Map<Integer, String> getClasses() {
		return classes;
	}

	public Map<Integer, String> getMethods() {
		return methods;
	}

	public Map<Integer, Integer> getMethodsToClasses() {
		return methodsToClasses;
	}

	public Map<Integer, Set<Integer>> getMethodCalls() {
		return methodCalls;
	}

	public Map<Integer, Integer> getClassesToParents() {
		return classesToParents;
	}

	public Map<Integer, Set<Metric>> getClassMetrics() {
		return classMetrics;
	}

	public Map<Integer, Set<Metric>> getMethodMetrics() {
		return methodMetrics;
	}

    public Map<Integer, Integer> getAttributesToParents() {
        return attributesToParents;
    }

    public Map<Integer, Integer> getAttributesToTypes() {
        return attributesToTypes;
    }

    public Map<Integer, String> getAttributes() {
        return attributes;
    }

    public Map<Integer, Integer> getMethodsToParameters() {
        return methodsToParameters;
    }
}
