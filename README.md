# README #
### MSE2JSON ###

* MSE2JSON will convert an MSE FAMIX description of a software system into an equivalent JSON file.
* Version 1.0

### Set up ###

The entry point is MSE2Json. As program argument, provide the path pointing to the MSE file you wish to convert. The result will be a JSON file in the same location with the same name (albeit with a JSON suffix).

### Contact ###

* Contact Neil Walkinshaw (nw91@le.ac.uk) with any questions.

### MIT License ###

Copyright (c) 2015 Neil Walkinshaw, Simon Scarle

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.